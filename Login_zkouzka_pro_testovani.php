<?php
session_start();
require 'vendor/autoload.php';

class LoginHandler
{
    private $latte;
    private $connection;

    public function __construct()
    {
        $this->latte = new Latte\Engine;
        $this->latte->setTempDirectory('temp');
        $this->connectDatabase();
    }

    private function connectDatabase()
    {
        $this->connection = mysqli_connect("localhost", "root", "", "travelblog");
        if (mysqli_connect_errno()) {
            die('Database connection failed: ' . mysqli_connect_error());
        }
    }

    public function handleRequest()
    {
        if (isset($_POST['loginSubmit'])) {
            $this->processLogin();
        } else {
            $this->showLoginForm();
        }
    }

    private function processLogin()
    {
        $email = $_POST['email'];
        $password = $_POST['password'];

        if ($this->isInputEmpty($email, $password)) {
            $this->redirect('login.php?error=inputnotset');
        }

        $user = $this->getUserByEmail($email);

        if (!$user) {
            $this->redirect('login.php?error=uzivatelnebylnalezen');
        }

        if ($this->verifyPassword($password, $user['Pasword'])) {
            $this->loginUser($user);
            $this->redirect('index.php?error=jstePrihlaseni');
        } else {
            $this->redirect('login.php?error=spatneheslo');
        }
    }

    public function isInputEmpty($email, $password)
    {
        return empty($email) || empty($password);
    }

    public function getUserByEmail($email)
    {
        $query = "SELECT * FROM users WHERE UserEmail = '$email'";
        $result = mysqli_query($this->connection, $query);
        return mysqli_fetch_assoc($result);
    }

    public function verifyPassword($password, $hashedPassword)
    {
        return password_verify($password, $hashedPassword);
    }

    private function loginUser($user)
    {
        $_SESSION["id"] = $user['idUser'];

        $cookieName = "travelblog";
        $value = $user['idUsers'];
        $expiration = time() + (60 * 60 * 24);
        setcookie($cookieName, $value, $expiration);
    }

    private function redirect($url)
    {
        header('Location:' . $url);
        exit();
    }

    private function showLoginForm()
    {
        $this->latte->render('templates/login.latte');
    }
}

//$loginHandler = new LoginHandler();
//$loginHandler->handleRequest();


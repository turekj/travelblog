-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Čtv 25. dub 2024, 08:41
-- Verze serveru: 10.4.32-MariaDB
-- Verze PHP: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `travelblog`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `articles`
--

CREATE TABLE `articles` (
  `idArticles` int(11) NOT NULL,
  `Title` varchar(120) NOT NULL,
  `Content` longtext NOT NULL,
  `ProfileImg` varchar(45) DEFAULT NULL,
  `Author` int(11) NOT NULL,
  `Destination` int(11) NOT NULL,
  `DatePublic` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `articles`
--

INSERT INTO `articles` (`idArticles`, `Title`, `Content`, `ProfileImg`, `Author`, `Destination`, `DatePublic`) VALUES
(1, 'Šumavské slatě', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque pretium lectus id turpis. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Nulla quis diam. In enim a arcu imperdiet malesuada. Nulla pulvinar eleifend sem. Aenean placerat. Aliquam erat volutpat. In convallis. Phasellus faucibus molestie nisl. Suspendisse nisl. Nulla non lectus sed nisl molestie malesuada. Maecenas lorem. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Proin mattis lacinia justo.', 'uploadImages/slateSumava.jpg', 2, 2, '2017-06-15'),
(2, 'Materhorn v noci', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque pretium lectus id turpis. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Nulla quis diam. In enim a arcu imperdiet malesuada. Nulla pulvinar eleifend sem. Aenean placerat. Aliquam erat volutpat. In convallis. Phasellus faucibus molestie nisl. Suspendisse nisl. Nulla non lectus sed nisl molestie malesuada. Maecenas lorem. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Proin mattis lacinia justo.', 'uploadImages/Matterhorn.jpg', 3, 4, '2017-07-15'),
(3, 'Šumava slatě II', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque pretium lectus id turpis. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Nulla quis diam. In enim a arcu imperdiet malesuada. Nulla pulvinar eleifend sem. Aenean placerat. Aliquam erat volutpat. In convallis. Phasellus faucibus molestie nisl. Suspendisse nisl. Nulla non lectus sed nisl molestie malesuada. Maecenas lorem. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Proin mattis lacinia justo.', 'uploadImages/slateSumava.jpg', 2, 2, '2017-07-15'),
(4, 'Jižní Tyrolsko', 'Severovýchod historického Jižního Tyrolska tvoří několik údolí v okolí města Bruneck (Brunico). Třetí největší město v Jižním Tyrolsku (po Brixenu a Bolzanu) leží na pomezí Dolomit a dalších pohoří východních Alp. Jedním z nich jsou nepříliš známé Villgratenské hory. Název dostaly podle údolí Villgratental, jenž leží na rakouské části jeho území. Menší část Villgratenských hor leží na území Itálie, kde je od Dolomit dělí údolí řeky Rienz (Bruneck, Toblach).\r\n\r\nVyhlášeným střediskem na druhé straně řeky je Kronplatz (Plan de Corones). Hora vysoká 2275 metrů je z východní poloviny obklopena vějířem sjezdovek všech obtížností. Na vrcholu je umístěn největší zvon v Alpách, Concordia 2000, a několikrát sem zabrousil i závod Giro d’Italia.', 'uploadImages/clanky/Jizni Tyrolsko.jpg', 2, 5, '2019-04-19'),
(5, 'Kalábrie', 'Kalábrie rozkládající se na samotném jihu Apeninského poloostrova je často označována „perlou jižní Itálie“. Oplývá krásnou a nedotčenou přírodou a je tvořena převážně hornatou krajinou s jedinečnými vyhlídkami. Průmysl zde téměř nenajdete, zatímco zemědělská činnost je v tomto regionu dosti patrná. Teplé a příjemné počasí panuje od dubna do října, ideální podmínky pro rekreaci se nabízí od května do července, kdy se teploty vzduchu pohybují okolo 30 °C, v srpnu mohou vyšplhat až na 35 °C. Právě v Kalábrii se nachází ty nejčistší úseky moře a pláží nejen v Itálii, ale dokonce i v celé Evropě. Od České republiky je Kalábrie vzdálená asi 1600 km. \r\n\r\nKalábrie je z turistického hlediska velmi atraktivní, nabízí spoustu lokalit, které stojí za vidění. Jednou z nich je například moderní turistické středisko Praia Mare s unikátní polohou u 5 km dlouhé vulkanické pláže. Nedaleký ostrov Dino je známý svými jeskyněmi, do kterých se pořádají lodní výlety. V této oblasti se nachází pobřežní útesy, které poskytují ideální podmínky pro potápění a šnorchlování. V Praia Mare nechybí možnosti nákupů, zábavy a sportovního vyžití. \r\n\r\n\r\n\r\nZa nejkrásnější oblast Kalábrie se považuje mys Capo Vaticano, kde najdete zlatavé písečné pláže, skalnaté útesy a křišťálově čisté moře. V blízkosti se nachází krásné historické město Tropea, jenž patří k  nejnavštěvovanějším v celé Kalábrii. Je plné zachovalých historických památek, jako je kostel sv. Marie, kostel sv. Františka z Assisi či normanská katedrála postavená v 11. století. Pokud byste se chtěli kochat výhledy na ostrov Dino a pobřeží s četnými zátokami, navštivte malé antické městečko San Nicola Arcella. Atraktivních míst je však v Kalábrii ještě mnohem více, vynechat byste neměli ani prohlídku městeček Diamante, Cetraro, Santa Maria del Cedro, Brancaleone, Scilla, Reggio Calabria, Pizzo, Cosenza nebo Scalea, které je jedním z největších a nejstarších na Riviera dei Cedri.', 'uploadImages/clanky/Kalabrie.jpg', 1, 5, '2021-02-09'),
(6, 'Benátky', 'Benátky (italsky Venezia [veˈnɛttsia]IPA, benátsky Venesia nebo Venexia [veˈnessi̯a]IPA) jsou hlavní město severoitalské oblasti Benátsko. Jeho historické jádro leží na ostrovech v mělké Benátské laguně, oddělené od Jaderského moře úzkou kosou Lido, novější části města byly vybudovány na pevnině (Mestre). Ve dnech zvaných „acqua alta“ (vysoká voda), kterých bývá každoročně průměrně 54, se zde zvýší hladina moře až o 90 cm.[2][3][4]\r\n\r\nKdysi námořní a obchodní velmoc a středisko sklářství, mají Benátky dodnes velký historický, kulturní, společenský i administrativní význam. Ve středověku byly centrem Benátské republiky, dnes jsou středem své oblasti, probíhá tu bohatý kulturní život. Nachází se zde velký přístav (na západním kraji) a letiště (na pevnině). Od roku 1987 jsou Benátky a celá okolní laguna jako jedna z nejnavštěvovanějších italských destinací památkou UNESCO. Město žije převážně z turistického ruchu.[5] Každý rok je navštíví přibližně 25 milionů turistů z celého světa a Benátky tak patří mezi nejnavštěvovanější města světa. Důležitou roli přitom hraje Biennale di Venezia, prestižní přehlídka současného výtvarného umění pořádaná v každém lichém roce.[6] Od roku 1932 se začal pořádat první mezinárodní filmový festival. V roce 1949 tu poprvé pro hlavní cenu použili název „Lev sv. Marka“, (o tři roky později se této ceně začalo říkat – a říká dosud „Zlatý lev sv. Marka“, vedle ní se tu také pokaždé uděluje i několik „Stříbrných lvů sv. Marka“).', 'uploadImages/clanky/Benatky.jpg', 1, 5, '2024-06-13'),
(7, 'Řím', 'Řím, hlavní město Itálie, je jedním z nejstarších a nejvíce fascinujících měst na světě. S více než 2 500 lety historie je Řím nabitý památkami, kulturou a uměním, které lákají miliony turistů každý rok.\r\n\r\nHistorie a památky\r\n	Řím byl kdysi centrem mocného římského impéria, což je stále vidět v jeho monumentálních stavbách, jako je Koloseum, Pantheon a Forum Romanum. Tyto historické památky poskytují návštěvníkům fascinující pohled do minulosti a ukazují, jaký vliv měl Řím na vývoj západní civilizace.\r\n\r\nKultura a umění\r\n	Kromě svého bohatého dědictví je Řím také centrem světového umění a kultury. V městě najdete nespočet muzeí, galerií a historických kostelů, včetně Vatikánských muzeí s Sixtinskou kaplí, kde můžete obdivovat díla mistrů jako Michelangelo a Rafael.\r\n\r\nGastronomie a kulinářský zážitek\r\n	Řím je také rájem pro milovníky dobrého jídla. Město je známé svou lahodnou italskou kuchyní, včetně těstovin, pizzy a vína. Nezapomeňte ochutnat tradiční římské speciality, jako jsou římské artičoky, carbonara a tiramisu.\r\n\r\nZávěr\r\n	Řím je město, které nabízí nesčetné možnosti pro objevování a poznávání. Ať už jste zde kvůli historii, umění, kultuře nebo gastronomii, Řím vás určitě nezklame. Je to místo, které je třeba zažít a které vám nabídne nezapomenutelný zážitek, který vás přivede zpět znovu a znovu.', 'uploadImages/clanky/Rim.jpg', 3, 5, '2023-06-30'),
(8, 'Sicílie', 'Sicílie, největší ostrov Středozemního moře a perla jižní Itálie, je fascinujícím směsí kultury, historie a přírodní krásy. Toto magické místo láká návštěvníky svými starobylými památkami, úchvatnými plážemi a bohatou kulinářskou tradicí.\r\n\r\nHistorie a památky\r\n	Sicílie je domovem některých z nejimpozantnějších historických památek v Evropě. V dávných časech byla ostrovním centrem mnoha civilizací, což je stále vidět v archeologických nalezištích, jako je údolí chrámů v Agrigentu a antické divadlo v Taormině. Navštivte také město Sirakúza, kde můžete obdivovat starověké řecké chrámy a památky.\r\n\r\nPřírodní krása\r\n	Sicílie je nejen o historii, ale také o úchvatných přírodních scenériích. Zasnoubte se s krásou sopky Etna, nejvyšší aktivní sopkou v Evropě, která nabízí úžasné trekkingové a lyžařské možnosti. Ostrov je také známý svými krásnými plážemi a azurovým mořem, ideálním pro sluneční koupání a relaxaci.\r\n\r\nKultura a umění\r\n	Sicílie má bohatou kulturu, která je vidět v jejích tradičních festivalech, folklórní hudbě a umění. Ostrov je také domovem několika vynikajících muzeí a galerií, kde můžete objevit díla sicilských umělců a artefakty z bohaté historie ostrova.\r\n\r\nGastronomie a kulinářský zážitek\r\n	Sicílie je rájem pro milovníky italské kuchyně. Zdejší kuchyně je známá svými autentickými a bohatými chutěmi, včetně čerstvého mořského ovoce, olivového oleje první třídy, arabských koření a sladkostí jako je cannoli a cassata. Nezapomeňte ochutnat místní speciality, jako jsou caponata, arancini a sicilská pizza.\r\n\r\nZávěr\r\n	Sicílie je ostrov plný kontrastů a překvapení, který nabízí návštěvníkům nezapomenutelný mix historie, kultury, přírody a gastronomie. Je to místo, které vás okouzlí svým kouzlem a přivede vás zpět, abyste objevili více z toho, co tento úžasný ostrov nabízí.', 'uploadImages/clanky/Sicilie.jpg', 2, 5, '2016-08-16'),
(9, 'Neapol', 'Neapol, pulsující metropole na pobřeží Tyrhénského moře, je jedním z nejživějších a nejautentičtějších měst v Itálii. Toto dynamické město, známé svým bohatým kulturním dědictvím, živou atmosférou a vynikající kuchyní, přináší návštěvníkům nezapomenutelný zážitek.\r\n\r\nKultura a atmosféra\r\n	Neapol je městem s vlastním charakterem a atmosférou. Procházejte se úzkými uličkami starého města, kde se mísí barokní paláce, historické kostely a živé trhy. Město je domovem mnoha vynikajících muzeí, jako je Národní archeologické muzeum, kde můžete obdivovat poklady z Pompejí a Herculaneaum.\r\n\r\nGastronomie a kulinářské lahůdky\r\n	Neapol je považována za kolébku italské pizzy, a není divu, že zdejší kulinářská scéna je ohromující. Navštivte tradiční pizzerie, kde si můžete vychutnat autentickou neapolskou pizzu, připravovanou podle tradičních receptů. Kromě pizzy můžete ochutnat také další místní speciality, jako jsou sfogliatella, mozzarella di bufala a espresso.\r\n\r\nPřírodní krása a okolí\r\n	Neapol je obklopena nádhernou přírodou a úchvatnými výhledy. Z města se nabízí výlety do vesnických oblastí Kampánie, kde můžete objevit malebná městečka, vinice a olivové háje. Nedaleko Neapole se také nachází národní park Vesuv, který je domovem slavného stejnojmenného vulkánu, a Amalfijské pobřeží s jeho úchvatnými klifovými městy a krásnými plážemi.\r\n\r\nZávěr\r\n	Neapol je městem plným kontrastů, kde se historie setkává s modernitou, a kde místní kultura a gastronomie vytvářejí jedinečnou a nezapomenutelnou atmosféru. Je to místo, které vás okouzlí svým kouzlem a nabídne vám autentický pohled do italského života, který je odlišný od klasických turistických destinací.', 'uploadImages/clanky/Neapol.jpeg', 2, 5, '2014-07-10'),
(10, 'San Marino\r\n', 'San Marino, malý ale vznešený stát ležící na vrcholcích Apenin v severní Itálii, je jediným prežívajícím městským státem v Evropě a jeden z nejstarších suverénních států na světě. Toto historické město nabízí návštěvníkům jedinečný pohled na minulost, nádherné výhledy a fascinující kulturu.\r\n\r\nHistorie a unikátní pozice\r\n	San Marino byl založen v 4. století a je pojmenován po svém zakladateli, svatém Marinovi. Město je domovem některých z nejstarších a nejlépe zachovalých středověkých hradů a opevnění v Evropě. Procházka úzkými uličkami a historickými náměstími vás přenese zpět v čase do doby rytířů a králů.\r\n\r\nÚchvatné výhledy a přírodní krása\r\n	Díky svému horskému umístění nabízí San Marino impozantní výhledy na okolní krajinu a Tyrhénské moře. Město je obklopeno nádhernou přírodou, což nabízí skvělé možnosti pro pěší turistiku, cyklistiku a další outdoorové aktivity.\r\n\r\nKultura a tradice\r\n	Navzdory své malé velikosti má San Marino bohatou kulturu a tradice, která je vidět v místní architektuře, umění a folklóru. Město je domovem několika vynikajících muzeí a galerií, kde můžete objevit díla sicilských umělců a artefakty z bohaté historie ostrova.\r\n\r\nGastronomie a kulinářské lahůdky\r\n	I když je San Marino malý, nabízí návštěvníkům bohatou kulinářskou scénu s vynikajícími restauracemi a tradičními jídly. Ochutnejte místní speciality, jako jsou tortelli di zucca (dýňové ravioli), piadina (placka) a Sanmarinské víno, které je známé svou jedinečnou chutí a kvalitou.\r\n\r\nZávěr\r\n	San Marino je skvostným klenotem střední Evropy, který přináší návštěvníkům jedinečný pohled na historii, kulturu a přírodu. Toto malé město, které je odlišné od klasických italských turistických destinací, je ideálním místem pro ty, kteří hledají autentický a nezapomenutelný zážitek.', 'uploadImages/clanky/San Marino.jpg', 2, 5, '2024-02-21'),
(11, 'Milán', 'Milán, metropole módy a designu ležící v severní Itálii, je pulzujícím srdcem země, kde se historie setkává s modernitou. Toto dynamické město, známé svým bohatým kulturním dědictvím, architektonickými skvosty a vynikající kuchyní, je jednou z nejživějších destinací v Evropě.\r\n\r\nMóda a design\r\n	Milán je považován za světové centrum módy a designu, což je vidět v jeho elegantních ulicích, luxusních buticích a prestižních designových studiích. Město je domovem nejznámějších italských módních domů, jako jsou Versace, Prada a Armani, a každoročně hostí mezinárodní módní události, jako je Milan Fashion Week.\r\n\r\nHistorie a architektura\r\n	Navzdory své moderní tváři má Milán bohatou historii a architektonické dědictví. Katedrála Duomo di Milano, jedna z největších gotických katedrál na světě, je ikonickým symbolem města. Kromě Duoma můžete v Miláně obdivovat mnoho dalších historických památek a paláců, jako je hrad Sforza, Navigli kanály a La Scala, jedno z nejprestižnějších operních domů na světě.\r\n\r\nKultura a umění\r\n	Milán je kulturním centrem Itálie s bohatou nabídkou muzeí, galerií a uměleckých institucí. Město je domovem několika vynikajících uměleckých sbírek, včetně Pinacoteca di Brera a Museo del Novecento, kde můžete objevit díla italských a evropských umělců od renesance po současnost.\r\n\r\nGastronomie a kulinářské lahůdky\r\n	Milánská kuchyně je stejně působivá jako jeho módní a kulturní scéna. Město je známé svými luxusními restauracemi, tradičními trattoriemi a stylovými kavárnami. Ochutnejte místní speciality, jako jsou risotto alla milanese, ossobuco alla milanese a panettone, slavný vánoční koláč, který se stal symbolem města.\r\n\r\nZávěr\r\n	Milán je město kontrastů, kde se starověké setkává s moderním, a kde móda, kultura a gastronomie tvoří nezapomenutelnou a jedinečnou atmosféru. Pro milovníky historie, umění, módy a dobrého jídla je Milán ideálním místem pro objevování a poznávání italského života v jeho nejlepší podobě.', 'uploadImages/clanky/Milan.jpg', 2, 5, '2023-12-28');

-- --------------------------------------------------------

--
-- Struktura tabulky `destination`
--

CREATE TABLE `destination` (
  `idDestination` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `destination`
--

INSERT INTO `destination` (`idDestination`, `Name`) VALUES
(1, 'Vysoké Tatry'),
(2, 'Šumava'),
(3, 'Norsko'),
(4, 'Švýcarsko'),
(5, 'Itálie');

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `idUsers` int(11) NOT NULL,
  `UserName` varchar(45) NOT NULL,
  `User` varchar(45) NOT NULL,
  `UserEmail` varchar(45) NOT NULL,
  `Pasword` varchar(45) NOT NULL,
  `Role` set('admin','delegate') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`idUsers`, `UserName`, `User`, `UserEmail`, `Pasword`, `Role`) VALUES
(1, 'Admin', 'Administrátor', 'admin@travelblog.cz', 'efacc4001e857f7eba4ae781c2932dedf843865e', 'admin'),
(2, 'Delegat1', 'Karel Novák', 'novak@travelblog.cz', 'bec6d274aba05aeeb195b870b6c595c44b05c086', 'delegate'),
(3, 'Delegat2', 'Jana Malá', 'mala@travelblog.cz', '8b0e26325bc7b6ff6a3c7c573dd8dd35932b23cc', 'delegate');

--
-- Indexy pro exportované tabulky
--

--
-- Indexy pro tabulku `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`idArticles`),
  ADD KEY `Author_idx` (`Author`),
  ADD KEY `Destination_idx` (`Destination`);

--
-- Indexy pro tabulku `destination`
--
ALTER TABLE `destination`
  ADD PRIMARY KEY (`idDestination`);

--
-- Indexy pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUsers`),
  ADD UNIQUE KEY `UserEmail_UNIQUE` (`UserEmail`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `articles`
--
ALTER TABLE `articles`
  MODIFY `idArticles` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pro tabulku `destination`
--
ALTER TABLE `destination`
  MODIFY `idDestination` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `idUsers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `Author` FOREIGN KEY (`Author`) REFERENCES `users` (`idUsers`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Destination` FOREIGN KEY (`Destination`) REFERENCES `destination` (`idDestination`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

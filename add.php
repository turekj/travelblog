<?php
    require 'vendor/autoload.php';

    $connection = mysqli_connect("localhost", "root", "", "travelblog");

    include 'includePHP/cookies.php';
    

    include 'includePHP/log-out.php';

    use Latte\Engine;
    $latte = new Engine;
    $latte->setTempDirectory('temp');

    $query = "SELECT * from destination";

    $resultDestinace = mysqli_query($connection, $query);
    
    
    if (isset($_POST['add'])) {
        $title = $_POST['title'];
        $content = $_POST['content'];
        $image = "uploadImages/clanky/".$_FILES['image']['name'];;
        $destination = $_POST['destination'];
        $date = new DateTime();
        $formattedDate = $date->format('Y-m-d H:i:s'); // převod na formát kompatibilní s databází

        $tempLocation = $_FILES['image']['tmp_name'];
        $filename = $_FILES['image']['name'];
        $targetPath = "uploadImages/clanky/" . $filename;

        if (move_uploaded_file($tempLocation, $targetPath)) {
            header('Location:add.php?error=zadny');
        } else {
            header('Location:add.php?error=velky');
        }

        $stmt = $connection->prepare("INSERT INTO articles (Title, Content, ProfileImg, Author, Destination, DatePublic) VALUES (?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("sssiis", $title, $content, $image, $id, $destination, $formattedDate); 
        $stmt->execute();
        $stmt->close();
    }
    
    

    $params = [
        'resultDestinace' => $resultDestinace,

        'resultUser' => $resultUser,
        'resultUserNav' => $resultUserNav,

        'idCookie' => $id,
    ];

    $latte->render('templates/add.latte', $params);
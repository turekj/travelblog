<?php
    require 'vendor/autoload.php';

    $connection = mysqli_connect("localhost", "root", "", "travelblog");

    include 'includePHP/cookies.php';
    

    include 'includePHP/log-out.php';

    use Latte\Engine;
    $latte = new Engine;
    $latte->setTempDirectory('temp');

    if (isset($_GET["filtrySubmit"])) {
        unset($_GET["destinace"]);
        unset($_GET["hledat"]);
    }

    $params = [

        'resultUser' => $resultUser,
        'resultUserNav' => $resultUserNav,

        'idCookie' => $id,
    ];

    $latte->render('templates/admin.latte', $params);
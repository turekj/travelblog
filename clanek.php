<?php
require 'vendor/autoload.php';
$connection = mysqli_connect("localhost", "root", "", "travelblog");

include 'includePHP/cookies.php';

use Latte\Engine;

$latte = new Engine;
$latte->setTempDirectory('temp');

if (isset($_COOKIE["travelblog"])) {
    $id = $_COOKIE['travelblog'];
}else {
    $id = false;
}

include 'includePHP/log-out.php';


if (!$connection) {
    die("Chyba připojení k databázi: " . mysqli_connect_error());
}

$sql = "SELECT articles.*, users.userName AS AuthorName, destination.Name AS DestinationName 
        FROM articles 
        LEFT JOIN users ON articles.Author = users.idUsers 
        LEFT JOIN destination ON articles.Destination = destination.idDestination 
        WHERE articles.idArticles = " . $_GET["id"];


$result = mysqli_query($connection, $sql);
if (!$result) {
    die("Chyba při provádění dotazu: " . mysqli_error($connection));
}

$article = mysqli_fetch_assoc($result);

$params = [
    'article' => $article,
    'idCookie' => $id,
    'resultUser' => $resultUser,
    'resultUserNav' => $resultUserNav,
];

$latte->render('templates/clanek.latte', $params);
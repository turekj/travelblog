<?php
    require 'vendor/autoload.php';

    $connection = mysqli_connect("localhost", "root", "", "travelblog");

    include 'includePHP/cookies.php';
    

    include 'includePHP/log-out.php';

    use Latte\Engine;
    $latte = new Engine;
    $latte->setTempDirectory('temp');

    if (isset($_GET["filtrySubmit"])) {
        unset($_GET["destinace"]);
        unset($_GET["hledat"]);
    }

    $query = "SELECT * FROM destination";
    
    $resultDestination = mysqli_query($connection, $query);
    if (isset($_GET["destinace"])&&!empty($_GET["hledat"])) {//splňuje obě podmínky
        $destinace = $_GET["destinace"];
        $hledat = $_GET["hledat"];
        $stmt = $connection->prepare("SELECT idArticles, Title, Content, ProfileImg, UserName, DatePublic FROM articles INNER JOIN users ON articles.Author = users.idUsers where articles.Destination = ? AND articles.Title = ?");
        $stmt->bind_param("is", $destinace, $hledat);
        $stmt->execute();
        $resultArticle = $stmt->get_result();
        if ($resultArticle->num_rows == 0) {
            $query = "SELECT idArticles, Title, Content, ProfileImg, UserName, DatePublic FROM articles INNER JOIN users ON articles.Author = users.idUsers";

            $resultArticle = mysqli_query($connection, $query);
        }
        //$x = "oba";
    }else if (isset($_GET["destinace"])) {//splňuje pouze zaškrtávání destinace

        $destinace = $_GET["destinace"];
        $stmt = $connection->prepare("SELECT idArticles, Title, Content, ProfileImg, UserName, DatePublic FROM articles INNER JOIN users ON articles.Author = users.idUsers where articles.Destination = ?");
        
            $stmt->bind_param("i", $destinace);
            $stmt->execute();
            $resultArticle = $stmt->get_result();
            //$x = "des";
    }else if (!empty($_GET["hledat"])) {//splňuje pouze vyhledávání
        $hledat = $_GET["hledat"];
        $stmt = $connection->prepare("SELECT idArticles, Title, Content, ProfileImg, UserName, DatePublic FROM articles INNER JOIN users ON articles.Author = users.idUsers where articles.Title = ?");
        $stmt->bind_param("s", $hledat);
        $stmt->execute();
        $resultArticle = $stmt->get_result();
        //$x = "hle";
    } else{
        $query = "SELECT idArticles, Title, Content, ProfileImg, UserName, DatePublic FROM articles INNER JOIN users ON articles.Author = users.idUsers";

        $resultArticle = mysqli_query($connection, $query);
        //$x = "els";
    }


    $params = [
        'resultArticle' => $resultArticle,
        'resultDestination' => $resultDestination,
        'resultUser' => $resultUser,
        'resultUserNav' => $resultUserNav,
        'idCookie' => $id,
    ];

    $latte->render('templates/clanky.latte', $params);
    //$latte->render('templates/mainPage.latte', $params);
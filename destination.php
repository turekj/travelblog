<?php
require 'vendor/autoload.php';

$connection = mysqli_connect("localhost", "root", "", "travelblog");

include 'includePHP/cookies.php';
include 'includePHP/log-out.php';

use Latte\Engine;
$latte = new Engine;
$latte->setTempDirectory('temp');

// Handle filters if they are set
if (isset($_GET["filtrySubmit"])) {
    unset($_GET["destinace"]);
    unset($_GET["hledat"]);
}

// Handle delete request
if (isset($_POST['deleteDestination'])) {
    $idDestinationToDelete = $_POST['idDestination'];
    $stmt = $connection->prepare("DELETE FROM destination WHERE idDestination = ?");
    $stmt->bind_param("i", $idDestinationToDelete);
    $stmt->execute();
}

// Handle add request
if (isset($_POST['addDestination'])) {
    $newDestinationName = $_POST['newDestinationName'];
    $stmt = $connection->prepare("INSERT INTO destination (Name) VALUES (?)");
    $stmt->bind_param("s", $newDestinationName);
    $stmt->execute();
}

// Query destinations
$query = "SELECT * FROM destination";
$resultDestination = mysqli_query($connection, $query);

// Convert result to array
$destinations = [];
while ($row = mysqli_fetch_assoc($resultDestination)) {
    $destinations[] = $row;
}

// Pass data to Latte template
$params = [

    'destinations' => $destinations,
    'resultUser' => $resultUser,
    'resultUserNav' => $resultUserNav,
    'idCookie' => $id,
];

$latte->render('templates/admin/destination.latte', $params);
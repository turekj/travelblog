<?php
require 'vendor/autoload.php';
$connection = mysqli_connect("localhost", "root", "", "travelblog");

include 'includePHP/cookies.php';

use Latte\Engine;

$latte = new Engine;
$latte->setTempDirectory('temp');

if (isset($_COOKIE["travelblog"])) {
    $id = $_COOKIE['travelblog'];
} else {
    $id = false;
}

include 'includePHP/log-out.php';

if (!$connection) {
    die("Chyba připojení k databázi: " . mysqli_connect_error());
}

$articleId = $_GET["id"];

// Fetch the article data
$sql = "SELECT articles.*, users.userName AS AuthorName, destination.Name AS DestinationName 
        FROM articles 
        LEFT JOIN users ON articles.Author = users.idUsers 
        LEFT JOIN destination ON articles.Destination = destination.idDestination 
        WHERE articles.idArticles = " . $articleId;

$result = mysqli_query($connection, $sql);
if (!$result) {
    die("Chyba při provádění dotazu: " . mysqli_error($connection));
}

$article = mysqli_fetch_assoc($result);

// Fetch all authors
$authorsSql = "SELECT idUsers, userName FROM users";
$authorsResult = mysqli_query($connection, $authorsSql);
if (!$authorsResult) {
    die("Chyba při provádění dotazu: " . mysqli_error($connection));
}

$authors = [];
while ($row = mysqli_fetch_assoc($authorsResult)) {
    $authors[] = $row;
}

if (isset($_POST['update'])) {
    $title = $_POST['title'];
    $content = $_POST['content'];
    $author = $_POST['author'];
    $image = $article['ProfileImg'];

    if ($_FILES['img']['name']) {
        $tempLocation = $_FILES['img']['tmp_name'];
        $filename = basename($_FILES['img']['name']);
        $targetPath = "uploadImages/clanky/" . $filename;

        if (move_uploaded_file($tempLocation, $targetPath)) {
            $image = $targetPath;
        } else {
            die("Chyba při nahrávání obrázku.");
        }
    }

    $stmt = $connection->prepare("UPDATE articles SET Title = ?, Content = ?, ProfileImg = ?, Author = ? WHERE idArticles = ?");
    $stmt->bind_param("sssii", $title, $content, $image, $author, $articleId);

    if ($stmt->execute()) {
        header('Location:edit.php?id=' . $articleId . '&status=success');
    } else {
        die("Chyba při aktualizaci článku: " . mysqli_error($connection));
    }

    $stmt->close();
}

$params = [
    'article' => $article,
    'authors' => $authors,
    'idCookie' => $id,
    'resultUser' => $resultUser,
    'resultUserNav' => $resultUserNav,
];

$latte->render('templates/admin/edit.latte', $params);

<?php
session_start();
require 'vendor/autoload.php';
    $latte = new Latte\Engine;
    $latte->setTempDirectory('temp');
    
    
if (isset($_POST['loginSubmit'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];

    if (empty($email) || empty($password)) {
        header('Location:login.php?error=inputnotset');
        exit();
    }

    $connection = mysqli_connect("localhost", "root", "", "travelblog");

    $query = "SELECT * FROM users WHERE UserEmail = '$email'";

    $result = mysqli_query($connection, $query);

    $row = mysqli_fetch_assoc($result);
    if (!$row) {
        header('Location:login.php?error=uzivatelnebylnalezen');
        exit();
    }
        
        $hashovaneHeslo = $row['Pasword'];
        
        if (password_verify($password, $hashovaneHeslo)) {
            $_SESSION["id"] = $row['idUser'];
            
            $cookieName = "travelblog";
            $value = $row['idUsers'];
            $expiration = time() + (60*60*24);
            setcookie($cookieName, $value, $expiration);

            header('Location:index.php?error=jstePrihlaseni');
            exit();
        } else {
            header('Location:login.php?error=spatneheslo');
            exit();
        }
                

        
        
}


$latte->render('templates/login.latte');



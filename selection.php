<?php
require 'vendor/autoload.php';

$connection = mysqli_connect("localhost", "root", "", "travelblog");

include 'includePHP/cookies.php';
include 'includePHP/log-out.php';

use Latte\Engine;
$latte = new Engine;
$latte->setTempDirectory('temp');

// Handle filters if they are set
if (isset($_GET["filtrySubmit"])) {
    unset($_GET["destinace"]);
    unset($_GET["hledat"]);
}

// Query destinations
$query = "SELECT * FROM destination";
$resultDestination = mysqli_query($connection, $query);

// Filter articles based on search criteria

    $query = "SELECT idArticles, Title, Content, ProfileImg, UserName, DatePublic FROM articles INNER JOIN users ON articles.Author = users.idUsers";
    $resultArticle = mysqli_query($connection, $query);


// Pass data to Latte template
$params = [
    'resultArticle' => $resultArticle,
    'resultDestination' => $resultDestination,
    'resultUser' => $resultUser,
    'resultUserNav' => $resultUserNav,
    'idCookie' => $id,
];

$latte->render('templates/admin/selection.latte', $params);

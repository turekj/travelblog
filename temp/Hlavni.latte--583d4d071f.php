<?php

use Latte\Runtime as LR;

/** source: templates/Hlavni.latte */
final class Template583d4d071f extends Latte\Runtime\Template
{
	public const Source = 'templates/Hlavni.latte';


	public function main(array $ʟ_args): void
	{
		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="travelblog/uploadImages/logo.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/style.css">   

</head>
<body>
    <header>
            <a href="Hlavni.php">
                <h1>TRAVEL</h1>
                <img class="logo" src="uploadImages/logo.png" alt="Logo">
                <h1>EXPLORE</h1>
            </a>
    </header>
    <div id="fix">
        <hr>
        <nav>
            <a href="Hlavni.php">Domů</a>
            <a href="./PHP/places.php">Články</a>
            <a href="">Obrázky</a>
            <a href="">Místa</a>
            <a href="">Vyhledat</a>
            <a href="">Registrace</a>
            <a href="login.php">Přihlášení</a>
        </nav>
        <hr id="carka">
        
    <div class="slideshow-container">

        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide2.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide3.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide4.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide5.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide6.jpg" style="width:100%">
        </div>
    </div>
    <br>
    
    <div>
        <div>
        <p>dawdawdiua</p>
        </div>
    
        <div>
        <p>stranka</p>
        </div>
    
    
    
    
    </div>
   

   </div>
        <div id="footerdiv">                
        <hr>
            <footer>
                <div id="foot">
                    <a href="tel:+420123456789">Tel. číslo: +420 733 658 244</a><br>
                    <a href="mailto:info@travelexplore.com">Email: info@travelexplore.com</a>
                </div>
                <div class="social-icons">
                    <a href=""><img class="FooterIMG" src="uploadImages/instagram.png" alt="Instagram"></a>
                    <a href=""><img class="FooterIMG" src="uploadImages/facebook.png" alt="Facebook"></a>
                </div>
            </footer>
        </div>
    </div>
    
    <script src="SCRIPT/slideshow.js"></script>
    
</body>
</html>';
	}
}

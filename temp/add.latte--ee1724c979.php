<?php

use Latte\Runtime as LR;

/** source: templates/add.latte */
final class Templateee1724c979 extends Latte\Runtime\Template
{

	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo_white.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/add.css">
    <link rel="stylesheet" href="CSS/nav.css">
</head>
<body>
<div id="flex_ham">
    <header>
        <a href="index.php">
            <h1>TRAVEL</h1>
            <img class="logo" src="uploadImages/logo.png" alt="Logo">
            <h1>EXPLORE</h1>
        </a>
    </header>
    <div class="off-screen-menu">
    <ul>
        <li><a href="index.php">Domů</a></li>
        <li><a href="clanky.php">Články</a></li>
        <li><a href="">Obrázky</a></li>
        <li><a href="">Místa</a></li>
';
		if ($idCookie) /* line 29 */ {
			echo '        <li><a href="add.php">Přidat článek</a></li>
';
			while ($row = mysqli_fetch_assoc($resultUserNav)) /* line 31 */ {
				if ($row['idUsers'] == $idCookie) /* line 32 */ {
					echo '                <hr class="carkaNav">
                <li>';
					echo LR\Filters::escapeHtmlText($row['User']) /* line 34 */;
					echo '</li>
                <form action="" method="post">
                    <li><input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se"></li>
                </form>
';
					if ($row['Role'] == 'admin' || $user['Role'] == 'delegate') /* line 38 */ {
						echo '                    <li><a href="admin.php">Administrace</a></li>
';
					}
				}

			}
		} else /* line 43 */ {
			echo '        <li><a class="login-registrace" href="login.php">Přihlášení</a></li>
        <li><a class="login-registrace" href="registrace.php">Registrace</a></li>
';
		}
		echo '    </ul>
</div>
    <div class="ham-menu">
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
    <div id="fix">
        <hr>
        <form action="" method="post" id="userForm">
            <nav>
                <a href="index.php">Domů</a>
                <a href="clanky.php">Články</a>
                <a href="">Obrázky</a>
                <a href="">Místa</a>
';
		if ($idCookie) /* line 63 */ {
			echo '                <a href="add.php">Přidat článek</a>
';
		}
		echo '            </nav>
            <div id="userIconDiv">
                <img src="uploadImages/userIcon.png" id="userIcon" alt="">
                <div id="dropdown">
';
		if ($idCookie) /* line 70 */ {
			while ($user = mysqli_fetch_assoc($resultUser)) /* line 71 */ {
				if ($user['idUsers'] == $idCookie) /* line 72 */ {
					echo '                        ';
					echo LR\Filters::escapeHtmlText($user['User']) /* line 73 */;
					echo '
                        <hr>
                        <input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se">
';
					if ($user['Role'] == 'admin') /* line 76 */ {
						echo '                        <hr>
                        <a href="admin.php">Administrace</a>
';
					}
				}

			}
		} else /* line 82 */ {
			echo '                    <a class="login-registrace" href="login.php">Přihlášení</a>
                    <hr>
                    <a class="login-registrace" href="registrace.php">Registrace</a>
';
		}
		echo '                </div>
            </div>
        </form>
    <hr id="carka">
    <div id="box">
        <form action="add.php" method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
            <label for="title">Nadpis:</label>
            <input type="text" id="title" name="title" required><br><br>
            <label for="content">Text:</label>
            <textarea id="content" name="content" rows="4" cols="50" required></textarea><br><br>
            <label for="image">Obrázek:</label>
            <input type="file" id="image" name="image" accept="image/*" required><br><br>
            <label for="destination">Destinace:</label>
            <select id="destination" name="destination" required>
';
		while ($row = mysqli_fetch_assoc($resultDestinace)) /* line 101 */ {
			echo '                <option value="';
			echo LR\Filters::escapeHtmlAttr($row['idDestination']) /* line 102 */;
			echo '">';
			echo LR\Filters::escapeHtmlText($row['Name']) /* line 102 */;
			echo '</option>
';

		}
		echo '            </select><br><br>
            <input id="button" type="submit" name="add" value="Přidat článek">
        </form>
    </div>
</div>
<script src="SCRIPT/nav.js"></script>
</body>
</html>
';
	}
}

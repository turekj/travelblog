<?php

use Latte\Runtime as LR;

/** source: templates/admin/edit.latte */
final class Template8cac7f6474 extends Latte\Runtime\Template
{

	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo_white.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/edit.css">
    <link rel="stylesheet" href="CSS/nav.css">
</head>
<body>
<div id="flex_ham">
    <header>
        <a href="admin.php">
            <h1>TRAVEL</h1>
            <img class="logo" src="uploadImages/logo.png" alt="Logo">
            <h1>EXPLORE</h1>
        </a>
    </header>
    <div class="off-screen-menu">
        <ul>
            <li><a href="edit.php">Úprava Článků</a></li>
            <li><a href="destination.php">Správa destinací</a></li>
            <li><a href="users.php">Správa uživatelů</a></li>
            <li><a href="index.php">Odejít z Administrace</a></li>
';
		if ($idCookie) /* line 29 */ {
			while ($row = mysqli_fetch_assoc($resultUserNav)) /* line 30 */ {
				if ($row['idUsers'] == $idCookie) /* line 31 */ {
					echo '                        <hr class="carkaNav">
                        <li>';
					echo LR\Filters::escapeHtmlText($row['User']) /* line 33 */;
					echo '</li>
                        <form action="" method="post">
                            <li><input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se"></li>
                        </form>
';
				}

			}
		} else /* line 39 */ {
			echo '                <li><a class="login-registrace" href="login.php">Přihlášení</a></li>
                <li><a class="login-registrace" href="registrace.php">Registrace</a></li>
';
		}
		echo '        </ul>
    </div>
    <div class="ham-menu">
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<div id="fix">
    <hr>
    <form action="" method="post" id="userForm">
        <nav>
            <a href="edit.php">Úprava článků</a>
            <a href="destination.php">Správa destinací</a>
            <a href="users.php">Správa uživatelů</a>
            <a href="index.php">Odejít z Administrace</a>
        </nav>
        <div id="userIconDiv">
            <img src="uploadImages/userIcon.png" id="userIcon" alt="">
            <div id="dropdown">
';
		if ($idCookie) /* line 63 */ {
			while ($user = mysqli_fetch_assoc($resultUser)) /* line 64 */ {
				if ($user['idUsers'] == $idCookie) /* line 65 */ {
					echo '                            ';
					echo LR\Filters::escapeHtmlText($user['User']) /* line 66 */;
					echo '
                            <hr>
                            <input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se">
';
					if ($user['Role'] == 'admin') /* line 69 */ {
						echo '                                <hr>
                                <a href="admin.php">Administrace</a>
';
					}
				}

			}
		} else /* line 75 */ {
			echo '                    <a class="login-registrace" href="login.php">Přihlášení</a>
                    <hr>
                    <a class="login-registrace" href="registrace.php">Registrace</a>
';
		}
		echo '            </div>
        </div>
    </form>
    <hr id="carka">

    <div id="box">
';
		if (isset($article)) /* line 86 */ {
			echo '            <form action="" method="post" enctype="multipart/form-data">
                <div>
                    <label for="title">Nadpis:</label>
                    <input type="text" id="title" name="title" value="';
			echo LR\Filters::escapeHtmlAttr($article['Title']) /* line 90 */;
			echo '">
                </div>
                <div>
                    <label for="content">Text:</label>
                    <textarea name="content" id="content">';
			echo LR\Filters::escapeHtmlText($article['Content']) /* line 94 */;
			echo '</textarea>
                </div>
                <div>
                    <label for="author">Autor:</label>
                    <select id="author" name="author">
                        <option value="';
			echo LR\Filters::escapeHtmlAttr($article['Author']) /* line 99 */;
			echo '">';
			echo LR\Filters::escapeHtmlText($article['AuthorName']) /* line 99 */;
			echo '</option>
';
			foreach ($authors as $author) /* line 100 */ {
				if ($author['idUsers'] != $article['Author']) /* line 101 */ {
					echo '                                <option value="';
					echo LR\Filters::escapeHtmlAttr($author['idUsers']) /* line 102 */;
					echo '">';
					echo LR\Filters::escapeHtmlText($author['userName']) /* line 102 */;
					echo '</option>
';
				}

			}

			echo '                    </select>
                </div>
                <div>
                    <label for="img">Obrázek:</label>
                    <input type="file" id="img" name="img">
                    <img id="image" src="';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($article['ProfileImg'])) /* line 110 */;
			echo '" alt="img">
                </div>
                <input type="submit" name="update" value="Aktualizovat článek">
            </form>
';
		} else /* line 114 */ {
			echo '            <p>Článek s daným ID nebyl nalezen.</p>
';
		}
		echo '    </div>
</div>
<script src="SCRIPT/nav.js"></script>
</body>
</html>
';
	}


	public function prepare(): array
	{
		extract($this->params);

		if (!$this->getReferringTemplate() || $this->getReferenceType() === 'extends') {
			foreach (array_intersect_key(['author' => '100'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		return get_defined_vars();
	}
}

<?php

use Latte\Runtime as LR;

/** source: templates/admin/users.latte */
final class Templateb3d7311291 extends Latte\Runtime\Template
{

	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo_white.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/users.css">
    <link rel="stylesheet" href="CSS/nav.css">
</head>
<body>
<div id="flex_ham">
    <header>
        <a href="admin.php">
            <h1>TRAVEL</h1>
            <img class="logo" src="uploadImages/logo.png" alt="Logo">
            <h1>EXPLORE</h1>
        </a>
    </header>
    <div class="off-screen-menu">
        <ul>
            <li><a href="selection.php">Úprava Článků</a></li>
            <li><a href="destination.php">Správa destinací</a></li>
            <li><a href="users.php">Správa uživatelů</a></li>
            <li><a href="index.php">Odejít z Administrace</a></li>
';
		if ($idCookie) /* line 29 */ {
			while ($row = mysqli_fetch_assoc($resultUserNav)) /* line 30 */ {
				if ($row['idUsers'] == $idCookie) /* line 31 */ {
					echo '                        <hr class="carkaNav">
                        <li>';
					echo LR\Filters::escapeHtmlText($row['User']) /* line 33 */;
					echo '</li>
                        <form action="" method="post">
                            <li><input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se"></li>
                        </form>
';
				}

			}
		} else /* line 39 */ {
			echo '                <li><a class="login-registrace" href="login.php">Přihlášení</a></li>
                <li><a class="login-registrace" href="registrace.php">Registrace</a></li>
';
		}
		echo '        </ul>
    </div>
    <div class="ham-menu">
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<div id="fix">
    <hr>
    <form action="" method="post" id="userForm">
        <nav>
            <a href="selection.php">Úprava článků</a>
            <a href="destination.php">Správa destinací</a>
            <a href="users.php">Správa uživatelů</a>
            <a href="index.php">Odejít z Administrace</a>
        </nav>
        <div id="userIconDiv">
            <img src="uploadImages/userIcon.png" id="userIcon" alt="">
            <div id="dropdown">
';
		if ($idCookie) /* line 63 */ {
			while ($user = mysqli_fetch_assoc($resultUser)) /* line 64 */ {
				if ($user['idUsers'] == $idCookie) /* line 65 */ {
					echo '                            ';
					echo LR\Filters::escapeHtmlText($user['User']) /* line 66 */;
					echo '
                            <hr>
                            <input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se">
';
					if ($user['Role'] == 'admin') /* line 69 */ {
						echo '                                <hr>
                                <a href="admin.php">Administrace</a>
';
					}
				}

			}
		} else /* line 75 */ {
			echo '                    <a class="login-registrace" href="login.php">Přihlášení</a>
                    <hr>
                    <a class="login-registrace" href="registrace.php">Registrace</a>
';
		}
		echo '            </div>
        </div>
    </form>
    <hr id="carka">
    <div class="table-container">
    
        <table>
            <thead>
                <tr>
                    <th>UserName</th>
                    <th>UserEmail</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
            </thead>
';
		while ($row = mysqli_fetch_assoc($resultUsers)) /* line 95 */ {
			echo '                <tbody>
                    <tr>
                        <td>';
			echo LR\Filters::escapeHtmlText($row['UserName']) /* line 98 */;
			echo '</td>
                        <td>';
			echo LR\Filters::escapeHtmlText($row['UserEmail']) /* line 99 */;
			echo '</td>
                        <td>
                            <form action="" method="post">
                                <select name="role" id="role">
                                    <option value="admin" ';
			if ($row['Role'] == 'admin') /* line 103 */ {
				echo 'selected';
			}
			echo '>admin</option>
                                    <option value="delegate" ';
			if ($row['Role'] == 'delegate') /* line 104 */ {
				echo 'selected';
			}
			echo '>delegate</option>
                                    
                                </select>
                                <input type="hidden" name="user_id" value="';
			echo LR\Filters::escapeHtmlAttr($row['idUsers']) /* line 107 */;
			echo '">
                                <input type="submit" name="change_role" value="změnit roli">
                            </form>
                        </td>
                        <td>
                            <form action="" method="post">
                                <input type="hidden" name="user_id" value="';
			echo LR\Filters::escapeHtmlAttr($row['idUsers']) /* line 113 */;
			echo '">
                                <input type="submit" name="delete_user" value="odstranit" id="delButton">
                            </form>
                        </td>
                    </tr>
                </tbody>
';

		}
		echo '        </table>
    </div>
</div>
<script src="SCRIPT/nav.js"></script>
</body>
</html>
';
	}
}

<?php

use Latte\Runtime as LR;

/** source: templates/admin.latte */
final class Templated0935c624e extends Latte\Runtime\Template
{

	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo_white.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/admin.css">
    <link rel="stylesheet" href="CSS/nav.css">

</head>
<body>
<div id="flex_ham">
    <header>
        <a href="admin.php">
                    <h1>TRAVEL</h1>
            <img class="logo" src="uploadImages/logo.png" alt="Logo">
            <h1>EXPLORE</h1>
        </a>
    </header>
    <div class="off-screen-menu">
    <ul>
        <li><a href="selection.php">Úprava Článků</a></li>
        <li><a href="">Správa destinací</a></li>
        <li><a href="users.php">Správa uživatelů</a></li>
        <li><a href="index.php">Odejít z Administrace</a></li>
';
		if ($idCookie) /* line 30 */ {
			echo "\n";
			while ($row = mysqli_fetch_assoc($resultUserNav)) /* line 32 */ {
				if ($row['idUsers'] == $idCookie) /* line 33 */ {
					echo '                                <hr class="carkaNav">
                                <li>';
					echo LR\Filters::escapeHtmlText($row['User']) /* line 35 */;
					echo '</li>
                                <form action="" method="post">
                                    <li><input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se"></li>
                                </form>
';
				}
				echo '                                
';

			}
			echo "\n";
		} else /* line 43 */ {
			echo '
                        <li><a class="login-registrace" href="login.php">Přihlášení</a></li>
                        
                        <li><a class="login-registrace" href="registrace.php">Registrace</a></li>
                        
';
		}
		echo '    </ul>
</div>
    <div class="ham-menu">
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
    <div id="fix">
        <hr>
        <form action="" method="post" id="userForm">
            <nav>
                <a href="selection.php">Úprava článků</a>
                <a href="">Správa destinací</a>
                <a href="users.php">Správa uživatelů</a>
                <a href="index.php">Odejít z Administrace</a>
            </nav>
            <div id="userIconDiv">
                <img src="uploadImages/userIcon.png" id="userIcon" alt="">
                <div id="dropdown">
';
		if ($idCookie) /* line 70 */ {
			echo "\n";
			while ($user = mysqli_fetch_assoc($resultUser)) /* line 72 */ {
				if ($user['idUsers'] == $idCookie) /* line 73 */ {
					echo '                                ';
					echo LR\Filters::escapeHtmlText($user['User']) /* line 74 */;
					echo '
                                    <hr>
                                    <input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se">
';
					if ($user['Role'] == 'admin') /* line 77 */ {
						echo '                                    <hr>
                                    <a href="admin.php">Administrace</a>
';
					}
				}
				echo '                                
';

			}
			echo "\n";
		} else /* line 85 */ {
			echo '
                        <a class="login-registrace" href="login.php">Přihlášení</a>
                        <hr>
                        <a class="login-registrace" href="registrace.php">Registrace</a>
                        
';
		}
		echo '                </div>
            </div>
        </form>
        <hr id="carka">
        <div id="flex">
            <div>
                <h1 id="nadpis">Výtejte v Administraci</h1>
                <h2 id="text">Zde můžete upravovat a přidávat své články, administrátor může spravovat uživatele</h2>
            </div>
            <img id="bird" src="uploadImages/bird-low-poly-mesh.png" alt="dfs">
        </div>
    <script src="SCRIPT/nav.js"></script>
</body>
</html>';
	}
}

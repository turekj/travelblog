<?php

use Latte\Runtime as LR;

/** source: templates/clanek.latte */
final class Template349d75bf36 extends Latte\Runtime\Template
{
	public const Source = 'templates/clanek.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="travelblog/uploadImages/logo.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/clanek_style.css">   

</head>
<body>
    <header>
            <a href="index.php">
                <h1>TRAVEL</h1>
                <img class="logo" src="uploadImages/logo.png" alt="Logo">
                <h1>EXPLORE</h1>
            </a>
    </header>
<div id="fix">
        <hr>
        <nav>
            <a href="index.php">Domů</a>
            <a href="./PHP/places.php">Články</a>
            <a href="">Obrázky</a>
            <a href="">Místa</a>
            <a href="">Vyhledat</a>
            <a href="">Registrace</a>
            <a href="login.php">Přihlášení</a>
        </nav>
        <hr id="carka">



';
		if (isset($article)) /* line 38 */ {
			echo '        <h2 id="title">';
			echo LR\Filters::escapeHtmlText($article['Title']) /* line 39 */;
			echo '</h2>
        <div id="box">
';
			if (isset($article['ProfileImg'])) /* line 41 */ {
				echo '                <img id="img_clanek" src="';
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($article['ProfileImg'])) /* line 42 */;
				echo '" alt="Obrázek k článku">
';
			}
			echo '            <p id="content">';
			echo LR\Filters::escapeHtmlText($article['Content']) /* line 44 */;
			echo '</p> 
            <p>Autor: ';
			echo LR\Filters::escapeHtmlText($article['AuthorName']) /* line 45 */;
			echo '</p>
            <p>Destinace: ';
			echo LR\Filters::escapeHtmlText($article['DestinationName']) /* line 46 */;
			echo '</p>
            <p>Datum publikace: ';
			echo LR\Filters::escapeHtmlText($article['DatePublic']) /* line 47 */;
			echo '</p>
        </div>
';
		} else /* line 49 */ {
			echo '        <p>Článek s daným ID nebyl nalezen.</p>
';
		}
		echo '


    <div id="footerdiv">                
        <hr>
            <footer>
                <div id="foot">
                    <a href="tel:+420123456789">Tel. číslo: +420 733 658 244</a><br>
                    <a href="mailto:info@travelexplore.com">Email: info@travelexplore.com</a>
                </div>
                <div class="social-icons">
                    <a href=""><img class="FooterIMG" src="uploadImages/instagram.png" alt="Instagram"></a>
                    <a href=""><img class="FooterIMG" src="uploadImages/facebook.png" alt="Facebook"></a>
                </div>
            </footer>
        </div>
    </div>
    
    <script src="SCRIPT/slideshow.js"></script>
    <script src="SCRIPT/text.js"></script>
</div>
</body>
</html>';
	}
}

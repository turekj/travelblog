<?php

use Latte\Runtime as LR;

/** source: templates/clanky.latte */
final class Template2527ae8b1a extends Latte\Runtime\Template
{
	public const Source = 'templates/clanky.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="travelblog/uploadImages/logo.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo_white.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/style.css">   

</head>
<body>
    <header>
            <a href="index.php">
                <h1>TRAVEL</h1>
                <img class="logo" src="uploadImages/logo.png" alt="Logo">
                <h1>EXPLORE</h1>
            </a>
    </header>
    <div id="fix">
        <hr>
        <nav>
            <a href="index.php">Domů</a>
            <a href="clanky.php">Články</a>
            <a href="">Obrázky</a>
            <a href="">Místa</a>
            <a href="">Vyhledat</a>
';
		if ($idCookie) /* line 31 */ {
			echo '                ';
			echo LR\Filters::escapeHtmlText($idCookie) /* line 32 */;
			echo '
                <form action="" method="post">
                    <input id="input_button" type="submit" name="log-outSubmit" value="odhlásit se">
                </form>
                
';
		} else /* line 37 */ {
			echo '            <a href="registrace.php">Registrace</a>
            <a href="login.php">Přihlášení</a>
';
		}
		echo '            
        </nav>
        <hr id="carka">
        
    <div class="slideshow-container">

        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide2.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide3.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide4.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide5.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide6.jpg" style="width:100%">
        </div>
    </div>
    <br>
    
    <div style="text-align:center">
      <span class="dot"></span> 
      <span class="dot"></span> 
      <span class="dot"></span>
      <span class="dot"></span> 
      <span class="dot"></span> 
    </div>
    <div id="text">
        <div class="seznam-clanek">
';
		while ($row = mysqli_fetch_assoc($resultArticle)) /* line 74 */ {
			echo '
                <div class="clanek">
                    <div class="clanek-text">
                        <div>
                            <h1>';
			echo LR\Filters::escapeHtmlText($row['Title']) /* line 79 */;
			echo '</h1>
                        </div>
                        <div class="content">
                            <p>';
			echo LR\Filters::escapeHtmlText(substr($row['Content'], 0, 300)) /* line 82 */;
			echo '...</p>
                        </div>
                        <div>
                            <p>';
			echo LR\Filters::escapeHtmlText($row['UserName']) /* line 85 */;
			echo '</p>
                        </div>
                        <div>
                            <p>';
			echo LR\Filters::escapeHtmlText($row['DatePublic']) /* line 88 */;
			echo '</p>
                        </div>
                    </div>

                    <div class="centerIMG">
                        <form>
                            <img class="pokus" src="';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($row['ProfileImg'])) /* line 94 */;
			echo '" alt="">
                            <a id="img_button" href="clanek.php?id=';
			echo LR\Filters::escapeHtmlAttr($row['idArticles']) /* line 95 */;
			echo '">Přejít na článek</a>
                        </form>
                    </div>
                </div>
            
';

		}
		echo '        </div>

            <fieldset id="search">
                <legend>Vyhledávání</legend>
                <form action="" method="post">
                    <input type="text" name="hledat" id="hledat" value="">
';
		while ($destination = mysqli_fetch_assoc($resultDestination)) /* line 107 */ {
			echo '                            <input class="seznam" type="radio" name="destinace"  value="';
			echo LR\Filters::escapeHtmlAttr($destination['idDestination']) /* line 108 */;
			echo '"> <label for="';
			echo LR\Filters::escapeHtmlAttr($destination['Name']) /* line 108 */;
			echo '">';
			echo LR\Filters::escapeHtmlText($destination['Name']) /* line 108 */;
			echo '</label><br>
';

		}
		echo '                    <input id="input_button" type="submit" name="destinaceSubmit" value="Hledat">
                </form>
              </fieldset>   
            
    </div>
        <div id="footerdiv">                
        <hr>
            <footer>
                <div id="foot">
                    <a href="tel:+420123456789">Tel. číslo: +420 733 658 244</a><br>
                    <a href="mailto:info@travelexplore.com">Email: info@travelexplore.com</a>
                </div>
                <div class="social-icons">
                    <a href=""><img class="FooterIMG" src="uploadImages/instagram.png" alt="Instagram"></a>
                    <a href=""><img class="FooterIMG" src="uploadImages/facebook.png" alt="Facebook"></a>
                </div>
            </footer>
        </div>
    </div>
    
    <script src="SCRIPT/slideshow.js"></script>
    <script src="SCRIPT/text.js"></script>

</body>
</html>';
	}
}

<?php

use Latte\Runtime as LR;

/** source: templates/place.latte */
final class Template4c38f77152 extends Latte\Runtime\Template
{

	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="travelblog/uploadImages/logo.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="../uploadImages/logo.png" type="image/x-icon">
    <title>TRAVEL EXPLORE - Places</title>
    <link rel="stylesheet" href="../CSS/style.css">   

</head>
<body>
    <header>
            <a href="../index.php">
                <h1>TRAVEL</h1>
                <img class="logo" src="../uploadImages/logo.png" alt="Logo">
                <h1>EXPLORE</h1>
            </a>
    </header>
    <div id="fix">
        <hr>
        <nav>
            <a href="../index.php">Domů</a>
            <a href="places.php">Články</a>
            <a href="">Obrázky</a>
            <a href="">Místa</a>
            <a href="">Vyhledat</a>
            <a href="">Registrace</a>
            <a href="../login.php">Přihlášení</a>
        </nav>
        <hr id="carka">
    
    <form action="search.php" method="get" class="HledatOkno">
        <input class="vyhledat" type="text" name="query" placeholder="Hledat...">
        <input class="BTvyhledat" type="submit" value="">
    </form>

    <div id="text">
        <div class="seznam-clanek">
';
		while ($row = mysqli_fetch_assoc($resultArticle)) /* line 43 */ {
			echo '                <div class="clanek">
                    <div class="clanek-text">
                        <div>
                            <h1>';
			echo LR\Filters::escapeHtmlText($row['Title']) /* line 47 */;
			echo '</h1>
                        </div>
                        <div>
                            <p>';
			echo LR\Filters::escapeHtmlText($row['Content']) /* line 50 */;
			echo '</p>
                        </div>
                        <div>
                            <p>';
			echo LR\Filters::escapeHtmlText($row['UserName']) /* line 53 */;
			echo '</p>
                        </div>
                    </div>
                </div>
';

		}
		echo '            

        </div>
    </div>
        <div id="footerdiv">                
        <hr>
            <footer>
                <div id="foot">
                    <a href="tel:+420123456789">Tel. číslo: +420 733 658 244</a><br>
                    <a href="mailto:info@travelexplore.com">Email: info@travelexplore.com</a>
                </div>
                <div class="social-icons">
                    <a href=""><img class="FooterIMG" src="../uploadImages/instagram.png" alt="Instagram"></a>
                    <a href=""><img class="FooterIMG" src="../uploadImages/facebook.png" alt="Facebook"></a>
                </div>
            </footer>
        </div>
    </div>
</body>
</html>';
	}
}

<?php

use Latte\Runtime as LR;

/** source: templates/register_page.latte */
final class Template1341de9461 extends Latte\Runtime\Template
{

	public function main(array $ʟ_args): void
	{
		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="travelblog/uploadImages/logo.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo_white.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/register_page.css">   
</head>
<body>
    <div id="login_box">
        <div id="login">
            <form method="post">
                <a href="index.php" id="login_head">
                    <img id="logo" src="uploadImages/logo_white.png" alt="logo">
                    <h1>Přihlášení</h1>
                </a>
                <input type="text" placeholder="Uživatelské jméno" id="userName">
                <br>
                <input type="text" placeholder="Jméno" id="name">
                <br>
                <input type="text" placeholder="Příjmení" id="surname">
                <br>
                <input type="text" placeholder="Email" id="email">
                <br>
                <input type="text" placeholder="Heslo" id="password">
                <br>
                <a href="login_page.php" id="registration_button">Přihlásit se</a>
                <br>
                <input type="submit" value="Registrovat">
            </form>
        </div>
    </div>
</body>
</html>';
	}
}

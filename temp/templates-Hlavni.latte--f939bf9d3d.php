<?php

use Latte\Runtime as LR;

/** source: templates/Hlavni.latte */
final class Template_f939bf9d3d extends Latte\Runtime\Template
{
	public const Source = 'templates/Hlavni.latte';


	public function main(array $ʟ_args): void
	{
		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="travelblog/uploadImages/logo.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/Hlavni.css">   

</head>
<body>

<script>

// Funkce pro spuštění animace při doscrollování
function startAnimation() {
    var elements = document.querySelectorAll(\'.textH\');
    elements.forEach(function(el) {
        if (isElementInViewport(el)) {
            el.style.animation = \'slideInRight 1s ease forwards\';
        }
    });
}

// Spuštění funkce při doscrollování
window.addEventListener(\'scroll\', startAnimation);


</script>

    <header>
            <a href="Hlavni.php">
                <h1>TRAVEL</h1>
                <img class="logo" src="uploadImages/logo.png" alt="Logo">
                <h1>EXPLORE</h1>
            </a>
    </header>
    <div id="fix">
        <hr>
        <nav>
            <a href="Hlavni.php">Domů</a>
            <a href="./PHP/places.php">Články</a>
            <a href="">Obrázky</a>
            <a href="">Místa</a>
            <a href="">Vyhledat</a>
            <a href="">Registrace</a>
            <a href="login.php">Přihlášení</a>
        </nav>
        <hr id="carka">
        
    <div class="slideshow-container">

        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide2.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide3.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide4.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide5.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide6.jpg" style="width:100%">
        </div>
    </div>
    <br>
    
    <div class="slozka">
        <div class="textH1">
        <p>
             <h2>Vítejte na našem travel blogu:</h2>
            <h4>Objevujeme svět společně!</h4>
            Vstupte do světa dobrodružství, objevů a nekonečných možností, které cestování nabízí. Vítáme vás na našem blogu, kde se vášeň pro objevování spojuje s touhou sdílet inspiraci a poznatky s ostatními cestovateli.
            <br>
            <br>
            Proč cestovat? Je to otázka, kterou se mnozí z nás často ptají. A odpověď? Možnosti jsou nekonečné. Cestování není jen o navštěvování nových míst a pozorování krásy světa - je to také o objevování sami sebe. Každá nová destinace, každé setkání s místními lidmi a každá kulturní zkušenost nám poskytuje příležitost růst a obohacovat naše životy.
            <br>
            <br>
            Naše cestovní dobrodružství nejsou jen o exotických destinacích a luxusních resortech. Jsou to příběhy o autentických zážitcích, místech mimo turistické trasy a nečekaných objevech. Sdílíme s vámi naše nejlepší tipy a triky, abychom vám pomohli plánovat své vlastní dobrodružství, ať už je vaším cílem objevit nové kouty světa nebo prozkoumat známá místa z nové perspektivy.
            <br>
            <br>
            Tak co, jste připraveni vydat se s námi na cestu? Nastavte si svůj kompas a připravte se na nezapomenutelné dobrodružství!</p>
        </div>
    
        <div class="textH2">
            <h2>Jak plánovat svou cestu jako profesionál</h2>
            <p>Plánování cesty může být stejně vzrušující jako samotná cesta. Pokud chcete mít úspěšný a bezproblémový výlet, je důležité předem dobře naplánovat. Zde jsou některé tipy, jak se stát profesionálem v plánování cest...</p>
            <ul>
                <li>Zvažte své cíle cestování a zájmy</li>
                <li>Vytvořte si rozpočet a plánujte své výdaje</li>
                <li>Vyberte si vhodný typ ubytování</li>
                <li>Prozkoumejte lokální kulturu a zvyky</li>
                <li>Připravte si balíček základních věcí</li>
                <li>Plánujte si aktivity a výlety předem</li>
                <li>Udržujte si flexibilitu a otevřenost</li>
            </ul>
            <p>S těmito tipy se stanete skutečným profesionálem v plánování cest a váš nadcházející výlet bude nezapomenutelným zážitkem!</p>
        </div>

    
    
    
    
    </div>
   

   </div>
        <div id="footerdiv">                
        <hr>
            <footer>
                <div id="foot">
                    <a href="tel:+420123456789">Tel. číslo: +420 733 658 244</a><br>
                    <a href="mailto:info@travelexplore.com">Email: info@travelexplore.com</a>
                </div>
                <div class="social-icons">
                    <a href=""><img class="FooterIMG" src="uploadImages/instagram.png" alt="Instagram"></a>
                    <a href=""><img class="FooterIMG" src="uploadImages/facebook.png" alt="Facebook"></a>
                </div>
            </footer>
        </div>
    </div>
    
    <script src="SCRIPT/slideshow.js"></script>
    
</body>
</html>';
	}
}

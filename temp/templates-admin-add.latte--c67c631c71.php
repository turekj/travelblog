<?php

use Latte\Runtime as LR;

/** source: templates/admin/add.latte */
final class Template_c67c631c71 extends Latte\Runtime\Template
{
	public const Source = 'templates/admin/add.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo_white.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/add.css">
    <link rel="stylesheet" href="CSS/nav.css">
</head>
<body>
<div id="flex_ham">
    <header>
        <a href="admin.php">
            <h1>TRAVEL</h1>
            <img class="logo" src="uploadImages/logo.png" alt="Logo">
            <h1>EXPLORE</h1>
        </a>
    </header>
    <div class="off-screen-menu">
        <ul>
            <li><a href="edit.php">Úprava Článků</a></li>
            <li><a href="add.php">Přidat článek</a></li>
            <li><a href="users.php">Správa uživatelů</a></li>
            <li><a href="index.php">Odejít z Administrace</a></li>
';
		if ($idCookie) /* line 29 */ {
			while ($row = mysqli_fetch_assoc($resultUserNav)) /* line 30 */ {
				if ($row['idUsers'] == $idCookie) /* line 31 */ {
					echo '                        <hr class="carkaNav">
                        <li>';
					echo LR\Filters::escapeHtmlText($row['User']) /* line 33 */;
					echo '</li>
                        <form action="" method="post">
                            <li><input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se"></li>
                        </form>
';
				}

			}
		} else /* line 39 */ {
			echo '                <li><a class="login-registrace" href="login.php">Přihlášení</a></li>
                <li><a class="login-registrace" href="registrace.php">Registrace</a></li>
';
		}
		echo '        </ul>
    </div>
    <div class="ham-menu">
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<div id="fix">
    <hr>
    <form action="" method="post" id="userForm">
        <nav>
            <a href="">Úprava článků</a>
            <a href="add.php">Přidat článek</a>
            <a href="users.php">Správa uživatelů</a>
            <a href="index.php">Odejít z Administrace</a>
        </nav>
        <div id="userIconDiv">
            <img src="uploadImages/userIcon.png" id="userIcon" alt="">
            <div id="dropdown">
';
		if ($idCookie) /* line 63 */ {
			while ($user = mysqli_fetch_assoc($resultUser)) /* line 64 */ {
				if ($user['idUsers'] == $idCookie) /* line 65 */ {
					echo '                            ';
					echo LR\Filters::escapeHtmlText($user['User']) /* line 66 */;
					echo '
                            <hr>
                            <input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se">
';
					if ($user['Role'] == 'admin') /* line 69 */ {
						echo '                                <hr>
                                <a href="admin.php">Administrace</a>
';
					}
				}

			}
		} else /* line 75 */ {
			echo '                    <a class="login-registrace" href="login.php">Přihlášení</a>
                    <hr>
                    <a class="login-registrace" href="registrace.php">Registrace</a>
';
		}
		echo '            </div>
        </div>
    </form>
    <hr id="carka">
    <div id="box">
        <form action="process_add_article.php" method="post" enctype="multipart/form-data">
            <label for="title">Nadpis:</label>
            <input type="text" id="title" name="title" required><br><br>
            <label for="content">Text:</label>
            <textarea id="text" id="content" name="content" rows="4" cols="50" required></textarea><br><br>
            <label for="image">Obrázek:</label>
            <input type="file" id="image" name="image" accept="image/*" required><br><br>
            <label for="destination">Destinace:</label>
            <select id="destination" name="destination" required>
                <option value="Destinace 1">Destinace 1</option>
                <option value="Destinace 2">Destinace 2</option>
                <option value="Destinace 3">Destinace 3</option>
            </select><br><br>
            <input id="button" type="submit" value="Přidat článek">
        </form>
    </div>
</div>
<script src="SCRIPT/nav.js"></script>
</body>
</html>
';
	}
}

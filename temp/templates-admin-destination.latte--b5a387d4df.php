<?php

use Latte\Runtime as LR;

/** source: templates/admin/destination.latte */
final class Template_b5a387d4df extends Latte\Runtime\Template
{
	public const Source = 'templates/admin/destination.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo_white.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/destination.css">
    <link rel="stylesheet" href="CSS/nav.css">

</head>
<body>
<div id="flex_ham">
    <header>
        <a href="admin.php">
                    <h1>TRAVEL</h1>
            <img class="logo" src="uploadImages/logo.png" alt="Logo">
            <h1>EXPLORE</h1>
        </a>
    </header>
    <div class="off-screen-menu">
    <ul>
        <li><a href="selection.php">Úprava Článků</a></li>
        <li><a href="destination.php">Správa destinací</a></li>
        <li><a href="users.php">Správa uživatelů</a></li>
        <li><a href="index.php">Odejít z Administrace</a></li>
';
		if ($idCookie) /* line 30 */ {
			while ($row = mysqli_fetch_assoc($resultUserNav)) /* line 31 */ {
				if ($row['idUsers'] == $idCookie) /* line 32 */ {
					echo '                                <hr class="carkaNav">
                                <li>';
					echo LR\Filters::escapeHtmlText($row['User']) /* line 34 */;
					echo '</li>
                                <form action="" method="post">
                                    <li><input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se"></li>
                                </form>
';
				}

			}
		} else /* line 40 */ {
			echo '                        <li><a class="login-registrace" href="login.php">Přihlášení</a></li>
                        
                        <li><a class="login-registrace" href="registrace.php">Registrace</a></li>
';
		}
		echo '    </ul>
</div>
    <div class="ham-menu">
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
    <div id="fix">
        <hr>
        <form action="" method="post" id="userForm">
            <nav>
                <a href="selection.php">Úprava článků</a>
                <a href="destination.php">Správa destinací</a>
                <a href="users.php">Správa uživatelů</a>
                <a href="index.php">Odejít z Administrace</a>
            </nav>
            <div id="userIconDiv">
                <img src="uploadImages/userIcon.png" id="userIcon" alt="">
                <div id="dropdown">
';
		if ($idCookie) /* line 65 */ {
			echo "\n";
			while ($user = mysqli_fetch_assoc($resultUser)) /* line 67 */ {
				if ($user['idUsers'] == $idCookie) /* line 68 */ {
					echo '                                ';
					echo LR\Filters::escapeHtmlText($user['User']) /* line 69 */;
					echo '
                                    <hr>
                                    <input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se">
';
					if ($user['Role'] == 'admin') /* line 72 */ {
						echo '                                    <hr>
                                    <a href="admin.php">Administrace</a>
';
					}
				}

			}
		} else /* line 78 */ {
			echo '                        <a class="login-registrace" href="login.php">Přihlášení</a>
                        <hr>
                        <a class="login-registrace" href="registrace.php">Registrace</a>                        
';
		}
		echo '                </div>
            </div>
        </form>
        <hr id="carka">
    <div id="flex">
        <div class="bor">
            <table class="uprava">
                <thead>
                    <tr>
                        <th>Destinace</th>
                        <th>Akce</th>
                    </tr>
                </thead>
                <tbody>
';
		foreach ($destinations as $destination) /* line 97 */ {
			echo '                        <tr>
                            <td>';
			echo LR\Filters::escapeHtmlText($destination['Name']) /* line 99 */;
			echo '</td>
                            <td>
                                <form class="delete-form" method="post">
                                    <input type="hidden" name="idDestination" value="';
			echo LR\Filters::escapeHtmlAttr($destination['idDestination']) /* line 102 */;
			echo '">
                                    <button class="delete-button" type="submit" name="deleteDestination">Odstranit</button>
                                </form>
                            </td>
                        </tr>
';

		}

		echo '                </tbody>
            </table>
        </div>
        <div class="bor">
            <h1>Přidat destinaci:</h1>
            <form method="post">
                <input type="text" name="newDestinationName" required>
                <button type="submit" name="addDestination">Přidat</button>
            </form>
        </div>
    </div>
</div>
    <script src="SCRIPT/nav.js"></script>
</body>
</html>';
	}


	public function prepare(): array
	{
		extract($this->params);

		if (!$this->getReferringTemplate() || $this->getReferenceType() === 'extends') {
			foreach (array_intersect_key(['destination' => '97'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		return get_defined_vars();
	}
}

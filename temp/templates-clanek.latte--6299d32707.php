<?php

use Latte\Runtime as LR;

/** source: templates/clanek.latte */
final class Template_6299d32707 extends Latte\Runtime\Template
{
	public const Source = 'templates/clanek.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="travelblog/uploadImages/logo.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo_white.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/clanek_style.css">   
    <link rel="stylesheet" href="CSS/nav.css">
</head>
<body>
<div id="flex_ham">
    <header>
        <a href="index.php">
                    <h1>TRAVEL</h1>
            <img class="logo" src="uploadImages/logo.png" alt="Logo">
            <h1>EXPLORE</h1>
        </a>
    </header>
    <div class="off-screen-menu">
    <ul>
        <li><a href="index.php">Domů</a></li>
        <li><a href="clanky.php">Články</a></li>
        <li><a href="">Obrázky</a></li>
        <li><a href="">Místa</a></li>
        <li><a href="">Vyhledat</a></li>
';
		if ($idCookie) /* line 31 */ {
			echo "\n";
			while ($user = mysqli_fetch_assoc($resultUser)) /* line 33 */ {
				if ($user['idUsers'] == $idCookie) /* line 34 */ {
					echo '                                ';
					echo LR\Filters::escapeHtmlText($user['User']) /* line 35 */;
					echo '
                                    
                                    <li><input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se"></li>
                                    
';
					if ($user['Role'] == 'admin') /* line 39 */ {
						echo '                                    
                                    <li><a href="admin.php">Administrace</a></li>

';
					}
				}
				echo '                                
';

			}
			echo "\n";
		} else /* line 48 */ {
			echo '
                        <li><a class="login-registrace" href="login.php">Přihlášení</a></li>
                        
                        <li><a class="login-registrace" href="registrace.php">Registrace</a></li>
                        
';
		}
		echo '    </ul>
</div>
    <div class="ham-menu">
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<div id="fix">
        <hr>
        <form action="" method="post" id="userForm">
            <nav>
                <a href="index.php">Domů</a>
                <a href="clanky.php">Články</a>
                <a href="">Obrázky</a>
                <a href="">Místa</a>
                <a href="">Vyhledat</a>
                        
                
            </nav>
            <div id="userIconDiv">
                <img src="uploadImages/userIcon.png" id="userIcon" alt="">
                <div id="dropdown">
';
		if ($idCookie) /* line 78 */ {
			echo "\n";
			while ($user = mysqli_fetch_assoc($resultUser)) /* line 80 */ {
				if ($user['idUsers'] == $idCookie) /* line 81 */ {
					echo '                                ';
					echo LR\Filters::escapeHtmlText($user['User']) /* line 82 */;
					echo '
                                    <hr>
                                    <input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se">
';
					if ($user['Role'] == 'admin') /* line 85 */ {
						echo '                                    <hr>
                                    <a href="admin.php">Administrace</a>
';
					}
				}
				echo '                                
';

			}
			echo '                        
                        
                
';
		} else /* line 95 */ {
			echo '                        <a class="login-registrace" href="login.php">Přihlášení</a>
                        <hr>
                        <a class="login-registrace" href="registrace.php">Registrace</a>
                        
                        
                        
                        
';
		}
		echo '                </div>
            </div>
            
        </form>
        <hr id="carka">



';
		if (isset($article)) /* line 112 */ {
			echo '        <h2 id="title">';
			echo LR\Filters::escapeHtmlText($article['Title']) /* line 113 */;
			echo '</h2>
        <div id="box">
';
			if (isset($article['ProfileImg'])) /* line 115 */ {
				echo '                <img id="img_clanek" src="';
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($article['ProfileImg'])) /* line 116 */;
				echo '" alt="Obrázek k článku">
';
			}
			echo '            <p id="content">';
			echo LR\Filters::escapeHtmlText($article['Content']) /* line 118 */;
			echo '</p> 
            <div id="description">
                <p>Autor: ';
			echo LR\Filters::escapeHtmlText($article['AuthorName']) /* line 120 */;
			echo '</p>
                <p>Destinace: ';
			echo LR\Filters::escapeHtmlText($article['DestinationName']) /* line 121 */;
			echo '</p>
                <p>Datum publikace: ';
			echo LR\Filters::escapeHtmlText($article['DatePublic']) /* line 122 */;
			echo '</p>
            </div>
        </div>
';
		} else /* line 125 */ {
			echo '        <p>Článek s daným ID nebyl nalezen.</p>
';
		}
		echo '


    <div id="footerdiv">                
        <hr>
            <footer>
                <div id="foot">
                    <a href="tel:+420123456789">Tel. číslo: +420 733 658 244</a><br>
                    <a href="mailto:info@travelexplore.com">Email: info@travelexplore.com</a>
                </div>
                <div class="social-icons">
                    <a href=""><img class="FooterIMG" src="uploadImages/instagram.png" alt="Instagram"></a>
                    <a href=""><img class="FooterIMG" src="uploadImages/facebook.png" alt="Facebook"></a>
                </div>
            </footer>
        </div>
    </div>
    <script src="SCRIPT/slideshow.js"></script>
    <script src="SCRIPT/nav.js"></script>
</div>
</body>
</html>';
	}
}

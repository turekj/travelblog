<?php

use Latte\Runtime as LR;

/** source: templates/clanky.latte */
final class Template_e3ceb25b87 extends Latte\Runtime\Template
{
	public const Source = 'templates/clanky.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="travelblog/uploadImages/logo.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo_white.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/style.css">
    <link rel="stylesheet" href="CSS/nav.css">

</head>
<body>
<div id="flex_ham">
    <header>
        <a href="index.php">
                    <h1>TRAVEL</h1>
            <img class="logo" src="uploadImages/logo.png" alt="Logo">
            <h1>EXPLORE</h1>
        </a>
    </header>
    <div class="off-screen-menu">
    <ul>
        <li><a href="index.php">Domů</a></li>
        <li><a href="clanky.php">Články</a></li>
        <li><a href="">Obrázky</a></li>
        <li><a href="">Místa</a></li>
        <li><a href="">Vyhledat</a></li>
        <li><a href="registrace.php">Registrovat se</a></li>
        <li><a href="login.php">Přihlásit se</a></li>
    </ul>
</div>
    <div class="ham-menu">
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
    <div id="fix">
        <hr>
        <form action="" method="post" id="userForm">
            <nav>
                <a href="index.php">Domů</a>
                <a href="clanky.php">Články</a>
                <a href="">Obrázky</a>
                <a href="">Místa</a>
                <a href="">Vyhledat</a>
                

            </nav>
            <div id="userIconDiv">
                <img src="uploadImages/userIcon.png" id="userIcon" alt="">
                <div id="dropdown">
';
		if ($idCookie) /* line 57 */ {
			echo "\n";
			while ($user = mysqli_fetch_assoc($resultUser)) /* line 59 */ {
				if ($user['idUsers'] == $idCookie) /* line 60 */ {
					echo '                                ';
					echo LR\Filters::escapeHtmlText($user['User']) /* line 61 */;
					echo '
                                    <hr>
                                    <input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se">
';
					if ($user['Role'] == 'admin') /* line 64 */ {
						echo '                                    <hr>
                                    <a href="admin.php">Administrace</a>
';
					}
				}
				echo '                                
';

			}
			echo "\n";
		} else /* line 72 */ {
			echo '
                        <a class="login-registrace" href="login.php">Přihlášení</a>
                        <hr>
                        <a class="login-registrace" href="registrace.php">Registrace</a>
                        
';
		}
		echo '                </div>
            </div>
            
        </form>
        <hr id="carka">
        
    <div class="slideshow-container">

        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide2.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide3.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide4.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide5.jpg" style="width:100%">
        </div>
        <div class="mySlides fade">
            <img src="uploadImages/slideshow/slide6.jpg" style="width:100%">
        </div>
    </div>
    <br>
    
    <div style="text-align:center">
      <span class="dot"></span> 
      <span class="dot"></span> 
      <span class="dot"></span>
      <span class="dot"></span> 
      <span class="dot"></span> 
    </div>
    <div id="text">
        <div class="seznam-clanek">
';
		while ($row = mysqli_fetch_assoc($resultArticle)) /* line 114 */ {
			echo '
                <div class="clanek">
                    <div class="clanek-text">
                        <div>
                            <h1>';
			echo LR\Filters::escapeHtmlText($row['Title']) /* line 119 */;
			echo '</h1>
                        </div>
                        <div class="content">
                            <p>';
			echo LR\Filters::escapeHtmlText(substr($row['Content'], 0, 300)) /* line 122 */;
			echo '...</p>
                        </div>
                        <div>
                            <p>';
			echo LR\Filters::escapeHtmlText($row['UserName']) /* line 125 */;
			echo '</p>
                        </div>
                        <div>
                            <p>';
			echo LR\Filters::escapeHtmlText($row['DatePublic']) /* line 128 */;
			echo '</p>
                        </div>
                    </div>

                    <div class="centerIMG">
                        <form>
                            <img class="pokus" src="';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($row['ProfileImg'])) /* line 134 */;
			echo '" alt="">
                            <a id="img_button" href="clanek.php?id=';
			echo LR\Filters::escapeHtmlAttr($row['idArticles']) /* line 135 */;
			echo '">Přejít na článek</a>
                        </form>
                    </div>
                </div>
            
';

		}
		echo '        </div>

            <fieldset id="search">
                <legend>Vyhledávání</legend>
                <form action="clanky.php" method="GET">
';
		if (isset($_GET['hledat'])) /* line 146 */ {
			echo '                        <input type="text" name="hledat" id="hledat" value="';
			echo LR\Filters::escapeHtmlAttr($_GET['hledat']) /* line 147 */;
			echo '">
';
		} else /* line 148 */ {
			echo '                        <input type="text" name="hledat" id="hledat" value="">
';
		}
		echo '                    
';
		while ($destination = mysqli_fetch_assoc($resultDestination)) /* line 152 */ {
			if (isset($_GET['destinace'])) /* line 153 */ {
				if ($_GET['destinace'] == $destination['idDestination']) /* line 154 */ {
					echo '                                <input class="seznam" type="radio" name="destinace" checked value="';
					echo LR\Filters::escapeHtmlAttr($destination['idDestination']) /* line 155 */;
					echo '" > <label for="';
					echo LR\Filters::escapeHtmlAttr($destination['Name']) /* line 155 */;
					echo '">';
					echo LR\Filters::escapeHtmlText($destination['Name']) /* line 155 */;
					echo '</label><br>
';
				} else /* line 156 */ {
					echo '                                <input class="seznam" type="radio" name="destinace" value="';
					echo LR\Filters::escapeHtmlAttr($destination['idDestination']) /* line 157 */;
					echo '" > <label for="';
					echo LR\Filters::escapeHtmlAttr($destination['Name']) /* line 157 */;
					echo '">';
					echo LR\Filters::escapeHtmlText($destination['Name']) /* line 157 */;
					echo '</label><br>
';
				}
			} else /* line 159 */ {
				echo '                            <input class="seznam" type="radio" name="destinace" value="';
				echo LR\Filters::escapeHtmlAttr($destination['idDestination']) /* line 160 */;
				echo '" > <label for="';
				echo LR\Filters::escapeHtmlAttr($destination['Name']) /* line 160 */;
				echo '">';
				echo LR\Filters::escapeHtmlText($destination['Name']) /* line 160 */;
				echo '</label><br>
';
			}

		}
		if (isset($_GET['hledat']) || isset($_GET['destinace'])) /* line 163 */ {
			echo '                        <input id="input_button" type="submit" name="filtrySubmit"  value="zrušit filtry">
';
		}
		echo '                    <input id="input_button" type="submit"  value="Hledat">
                </form>
              </fieldset>   
            
    </div>
        <div id="footerdiv">                
        <hr>
            <footer>
                <div id="foot">
                    <a href="tel:+420123456789">Tel. číslo: +420 733 658 244</a><br>
                    <a href="mailto:info@travelexplore.com">Email: info@travelexplore.com</a>
                </div>
                <div class="social-icons">
                    <a href=""><img class="FooterIMG" src="uploadImages/instagram.png" alt="Instagram"></a>
                    <a href=""><img class="FooterIMG" src="uploadImages/facebook.png" alt="Facebook"></a>
                </div>
            </footer>
        </div>
    </div>
    
    <script src="SCRIPT/slideshow.js"></script>
    <script src="SCRIPT/nav.js"></script>
</body>
</html>';
	}
}

<?php

use Latte\Runtime as LR;

/** source: templates/obrazky.latte */
final class Template_a26f812e93 extends Latte\Runtime\Template
{
	public const Source = 'templates/obrazky.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="travelblog/uploadImages/logo.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo_white.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/style.css">   

</head>
<body>
    <header>
            <a href="index.php">
                <h1>TRAVEL</h1>
                <img class="logo" src="uploadImages/logo.png" alt="Logo">
                <h1>EXPLORE</h1>
            </a>
    </header>
    <div id="fix">
        <hr>
        <form action="" method="post" id="userForm">
            <nav>
                <a href="index.php">Domů</a>
                <a href="clanky.php">Články</a>
                <a href="">Obrázky</a>
                <a href="">Místa</a>
                <a href="">Vyhledat</a>
                        
                
            </nav>
            <div id="userIconDiv">
                <img src="uploadImages/userIcon.png" id="userIcon" alt="">
                <div id="dropdown">
';
		if ($idCookie) /* line 38 */ {
			echo "\n";
			while ($user = mysqli_fetch_assoc($resultUser)) /* line 40 */ {
				if ($user['idUsers'] == $idCookie) /* line 41 */ {
					echo '                                ';
					echo LR\Filters::escapeHtmlText($user['User']) /* line 42 */;
					echo '
                                    <hr>
                                    <input id="odhlasit_button" type="submit" name="log-outSubmit" value="odhlásit se">
';
					if ($user['Role'] == 'admin') /* line 45 */ {
						echo '                                    <hr>
                                    <a href="admin.php">Administrace</a>
';
					}
				}
				echo '                                
';

			}
			echo "\n";
		} else /* line 53 */ {
			echo '
                        <a class="login-registrace" href="login.php">Přihlášení</a>
                        <hr>
                        <a class="login-registrace" href="registrace.php">Registrace</a>
                        
';
		}
		echo '                </div>
            </div>
            
        </form>
        <hr id="carka">
        <div id="footerdiv">                
        <hr>
            <footer>
                <div id="foot">
                    <a href="tel:+420123456789">Tel. číslo: +420 733 658 244</a><br>
                    <a href="mailto:info@travelexplore.com">Email: info@travelexplore.com</a>
                </div>
                <div class="social-icons">
                    <a href=""><img class="FooterIMG" src="uploadImages/instagram.png" alt="Instagram"></a>
                    <a href=""><img class="FooterIMG" src="uploadImages/facebook.png" alt="Facebook"></a>
                </div>
            </footer>
        </div>
    </div>
    
    <script src="SCRIPT/slideshow.js"></script>
    <script src="SCRIPT/text.js"></script>

</body>
</html>';
	}
}

<?php

use Latte\Runtime as LR;

/** source: templates/registrace.latte */
final class Template_55da66998d extends Latte\Runtime\Template
{
	public const Source = 'templates/registrace.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="travelblog/uploadImages/logo.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Azeret+Mono:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="icon" href="uploadImages/logo_white.png" type="image/x-icon">
    <title>TRAVEL EXPLORE</title>
    <link rel="stylesheet" href="CSS/register_page.css">   
</head>
<body>
    <div id="login_box">
        <div id="login">
            <form method="post">
                <a href="index.php" id="login_head">
                    <img id="logo" src="uploadImages/logo_white.png" alt="logo">
                    <h1>Registrace</h1>
                </a>
';
		if (isset($_GET['error'])) /* line 22 */ {
			$ʟ_switch = ($_GET['error']) /* line 23 */;
			if (in_array($ʟ_switch, ['inputnotset'], true)) /* line 24 */ {
				echo '                            <h3>Akce se nezdařila, zkuste to prosím znouvu</h3>
                            
';
			} elseif (in_array($ʟ_switch, ['rozdilneHesla'], true)) /* line 27 */ {
				echo '                            <h3>Hesla se neshodují</h3>
                            
';
			} elseif (in_array($ʟ_switch, ['userexists'], true)) /* line 30 */ {
				echo '                            <h3>Uživatel již existuje</h3>
                            
';
			} elseif (in_array($ʟ_switch, ['sqlerror'], true)) /* line 33 */ {
				echo '                            <h3>Akce se nezdařila, zkuste to prosím znouvu</h3>
                            
                                        
';
			}
			echo '                    <br>
';
		}
		echo '                <input type="text" placeholder="Uživatelské jméno" name="prezdivka" id="userName" required>
                <br>
                <input type="text" placeholder="Jméno" name="jmeno" id="name" required>
                <br>
                <input type="text" placeholder="Příjmení" name="prijmeni" id="surname" required>
                <br>
                <input type="email" placeholder="Email" name="email" id="email" required>
                <br>
                <input type="password" placeholder="Heslo" name="heslo1" id="password" required>
                <br>
                <input type="password" placeholder="Znovu Heslo" name="heslo2" id="password" required>
                <br>
                <a href="login.php" id="registration_button">Přihlásit se</a>
                <br>
                <input type="submit" name="registraceSubmit" value="Registrovat">
            </form>
        </div>
    </div>
</body>
</html>';
	}
}

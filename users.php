<?php
require 'vendor/autoload.php';

$connection = mysqli_connect("localhost", "root", "", "travelblog");


if (isset($_POST['change_role'])) {
    $user_id = $_POST['user_id'];
    $role = $_POST['role'];
    $stmt = $connection->prepare("UPDATE users SET Role = ? WHERE idUsers = ?");
    $stmt->bind_param("si", $role, $user_id);
    $stmt->execute();
    $stmt->close();
}

include 'includePHP/cookies.php';
include 'includePHP/log-out.php';

use Latte\Engine;
$latte = new Engine;
$latte->setTempDirectory('temp');



// Zpracování odstranění uživatele
if (isset($_POST['delete_user'])) {
    $user_id = $_POST['user_id'];
    $stmt = $connection->prepare("DELETE FROM users WHERE idUsers = ?");
    $stmt->bind_param("i", $user_id);
    $stmt->execute();
    $stmt->close();
}



$query = "SELECT * FROM users";
$resultUsers = mysqli_query($connection, $query);
    
$params = [
    'resultUser' => $resultUsers,

    'resultUsers' => $resultUser,
    'resultUserNav' => $resultUserNav,
    'idCookie' => $id,
    
];

$latte->render('templates/admin/users.latte', $params);
